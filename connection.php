<?php

// Creating a connection to the MySQL server.
// Learn : https://www.w3schools.com/php/php_mysql_connect.asp
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "chatbox";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

?>