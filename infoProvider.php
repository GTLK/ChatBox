<?php

    require_once("connection.php"); 

    $sql = "SELECT MAX(id) FROM messages;";
    if ($conn->query($sql) === FALSE) {
        echo "Error getting the last message ID: " . $conn->error;
        die();
    }

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo $row["MAX(id)"];
        }
    } else {
        echo "0";
    }
    
?>