<?php

// Learn about php connections here : https://www.w3schools.com/php/php_mysql_connect.asp

// This PHP script creates a database called chatbox and a table inside it called
// messages if they don't exist.

// This should be your loopback IP address ussually nobody has to change this as 
// long as you are running the MySQL server locally.
$servername = "127.0.0.1";

// This is where you define the user you prefer to access database as. root is the 
// default so I didn't change it at all. But there are some risks regarding security
// when it is done this way. But project should be simple.
// In case you want to change these stuff you can go to phpMyAdmin and click on 
// user accounts tab.
$username = "root";

// Here you should define the password for the above user. Default is empty.
$password = "";

// In PHP there are so many way that you can use to connect to MySQL. In this
// case I preffered this way. OOP approach. Sytax explains itself.
$conn = new mysqli($servername, $username, $password);


// Checking whether we were able to connect to the mysql connection.
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// https://www.w3schools.com/php/php_mysql_create.asp
// Creates the database if it doesn't exist.
$sql = "CREATE DATABASE IF NOT EXISTS chatbox";
if ($conn->query($sql) ===  FALSE) {
    echo "Error creating database: " . $conn->error;
    die();
}

$dbname = "chatbox";

// Connect to the newly created database.
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// https://www.w3schools.com/php/php_mysql_create_table.asp
// Creates the table for messages.
$sql = "CREATE TABLE IF NOT EXISTS messages (
    id          INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(id),
    DATE        DATETIME NOT NULL,
    NAME        VARCHAR(255),
    MESSAGE     VARCHAR(255)
);";

// Checks whether the table was created successfully.
if ($conn->query($sql) ===  FALSE) {
    echo "Error creating table: " . $conn->error;
    die();
}

?>

<!-- This bootstrap template is obtained trough https://getbootstrap.com/docs/3.3/getting-started/ -->
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ChatBox</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>

        <!-- Learn about the bootstrap fixed navigation bar here https://getbootstrap.com/docs/3.3/components/#navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">ChatBox</a>
                </div>
            </div>
        </nav>

        <!-- This is the dialog box which apperes when the site is loaded and asks for user name -->
        <!-- Learn about bootstrap modals here : https://v4-alpha.getbootstrap.com/components/modal/ -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ChatBox</h4>
                    </div>
                    <div class="modal-body">
                        <!-- Learn about bootstrap input groups here : https://getbootstrap.com/docs/3.3/components/#input-groups -->
                        <div class="input-group">
                            <input type="text" id="nameTextBox" class="form-control" placeholder="Enter your name here ...">
                            <span class="input-group-btn">
                                <!-- Learn about html events here : https://www.w3schools.com/jsref/event_onclick.asp -->
                                <button onClick="registerUser()" class="btn btn-default" type="button">Log In</button>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container" style="margin-top:70px;">
            
            <!-- Learn about bootstrap panels here : https://getbootstrap.com/docs/3.3/components/#panels -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    Messages
                </div>

                <!-- This list will include all the messages. -->
                <ul class="list-group" id="messagesList">
                    
                </ul>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="messageTextBox" placeholder="Type your message">
                                <span class="input-group-btn">
                                <!-- Learn about html events here : https://www.w3schools.com/jsref/event_onclick.asp -->                                
                                    <button onClick="sendMessage()" class="btn btn-default" type="button">Send</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <!-- This is where we send and receive messages using javascript -->
        <script src="js/index.js"></script>

    </body>
</html>