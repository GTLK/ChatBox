<?php

    require_once("connection.php");

    $max = $_POST["max"];
    $last = $_POST["last"];

    $loadNumberOfMessages = $max - $last;

    $sql = "SELECT * FROM (SELECT * FROM MESSAGES ORDER BY id DESC LIMIT ".$loadNumberOfMessages.") sub ORDER BY id ASC;";

    if ($conn->query($sql) === FALSE) {
        echo "Error getting the last message ID: " . $conn->error;
        die();
    }

    $result = $conn->query($sql);

    $output = "";

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $output .= '
            
                <li class="list-group-item">
                    <div class="input-group">
                        <span class="label label-default">'.$row["NAME"].'</span>
                        '.$row["MESSAGE"].'
                    </div>
                </li>
            
            ';
        }
    } else {
        die();
    }

    echo $output;

?>