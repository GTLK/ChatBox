// This variable is reserved for the username
var name;

// This variable is for the last message is available in our messages list
var lastMessageID = 0;

// This variable is reserved for last messages id in the database, so that we cant update the messages list
var id = 0

// This function updates the messages list within a given time interval
var updater = function updateMessageList(){

    // Learn Jquery GET : https://api.jquery.com/jquery.get/
    // infoProvider.php provides the last message is available in server database
    $.get("infoProvider.php", function(data, status){
        id = data
        console.log("Last Message: " + id);
    });

    // If there are new messages in the datebase load them to the list
    if (id > lastMessageID){
        console.log("Loading new messages !");
        $.post("messageLoader.php", {
            // Messagesloader.php requires 2 parameters
            last :lastMessageID,
            max : id,
        }, function(data, status){
            // Learn : http://api.jquery.com/append/
            $("#messagesList").append(data);
            lastMessageID = id;
        });

        // This will scroll the document when the new message is loaded.
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);          
    }
}

// Gets the username and starts receiving messages
function registerUser(){

    // Takes the value in name textbox in the bootstrap modal
    name = $("#nameTextBox").val();
    
    // Hides the modal
    $('#myModal').modal('toggle');

    // Changes the focus to message text box
    $("#messageTextBox").focus();    

    // this excute the updater function every 500 ms to receive messages.
    setInterval(updater , 500);    
}


// function to send messages.  this will be excuted when the user clicks on send button
function sendMessage(){

    // Receives the message in messagetextbox in the UI
    var message = $("#messageTextBox").val();

    // Appends the new message to the list
    // Learn https://getbootstrap.com/docs/4.0/components/list-group/
    $("#messagesList").append('\
        <li class="list-group-item"> \
            <div class="input-group" style="float:right"> \
            '+ message +' \
                <span class="label label-default">'+ name +'</span> \
            </div> \
            <div style="clear:both;"></div> \
        </li> \
    ');

    lastMessageID = parseInt(lastMessageID) + 1;

    // Learn : https://api.jquery.com/jquery.post/
    // Sends the message to messageSender.php
    $.post("messageSender.php",
        {
            name: name,
            message: message,
        },
        function(data, status){
            // alert(data);
        });

    // Empty the message box
    $("#messageTextBox").val("");

    // refocus to the messsage box
    $("#messageTextBox").focus();

    // Scroll to the bottom
    $("html, body").animate({ scrollTop: $(document).height() }, 1000);  
    
}

// Show the modal at the start up
$('#myModal').modal('toggle');

// Change the fosus to the user name text box 
$('#nameTextBox').focus();
